import MapReduce
import sys

"""
INPUT
    organism, protein, fasta
OUTPUT
    protein, (organism, fasta)

"""

mr = MapReduce.MapReduce()

# =============================
# Do not modify above this line

def mapper(record):
    # key: protein
    # value: (organism, fasta)
    key = record[1]
    value = (record[0], record[2])
    mr.emit_intermediate(key, value)


def reducer(key, list_of_values):
    # key: protein
    # value: ((organism, fasta), (organism, fasta), ...)
    mr.emit((key, list(set(list_of_values)) ))

# Do not modify below this line
# =============================
if __name__ == '__main__':
    inputdata = open(sys.argv[1])
    mr.execute(inputdata, mapper, reducer)
